/* File:   main.cpp
 * Author: Daniel Ormeño - S2850944
 * Griffith University - Gold Coast Campus
 * Data Structures & Algorithms Assignment 1
 * Main Calculator
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

class Calculator {
private:
    double result;
    string in;

public:

    Calculator() {
        result = 0;
        in = "";
    };

    // Operation METHODS

    double sum(double a, double b) {
        return a + b;
    };

    double substract(double a, double b) {
        return a - b;
    };

    double multiply(double a, double b) {
        return a*b;
    };

    double divide(double a, double b) {
        return a / b;
    };

    double power(double a, double b) {
        double aux = a;
        for (int j = 1; j < b; j++) {
            a = a*aux;
        }
        return a;
    }

    // Solve precedence METHODS

    void solvePrecedence(string in) {
        double count, count1, control, lowerIndex, higherIndex = 0;
        string operation = "";
        string input = in;

        if (in.size() > 0) {

            //Counts number of parenthesis
            for (int i = 0; i < input.size(); i++) {
                if (input[i] == '(') {
                    count++;
                } else if (input[i] == ')') {
                    count1++;
                }
            };

            //Checks that all parenthesis are in pairs
            if (count1 != count) {
                return;
            } else {
                //Finds most inner opening parenthesis .
                for (int j = 0; j < input.size(); j++) {
                    if (input[j] == '(') {
                        control++;
                        if (control == count) {
                            lowerIndex = j;
                            j = input.size();
                        }
                    }
                };
                control = 0;

                //Finds most inner closing parenthesis .
                for (int q = lowerIndex; q < input.size(); q++) {
                    if (input[q] == ')') {
                        higherIndex = q;
                        q = input.size();
                    }
                };
            }
            //Sets new string operation for solving inner parenthesis.

            if (higherIndex > lowerIndex) {
                for (int i = lowerIndex + 1; i < higherIndex; i++) {
                    operation += input[i];
                };
            }
            operation = solveOperation(operation); // Solves inner expression

            if (lowerIndex == 0)
                input = operation + input.substr(higherIndex + 1, input.size() - higherIndex - 1);
            else {
                operation = input.substr(0, lowerIndex) + operation + input.substr(higherIndex + 1, input.size() - higherIndex);
                
            }
            result=atof(operation.c_str());
            if(result>0)
            cout<<setprecision(3)<<result<<endl;
            solvePrecedence(operation);
        } else
            return;
    };

    string solveOperation(string in) {
        string aux = in;

        aux = operate(aux, '^');
        aux = operate(aux, '*');
        aux = operate(aux, '/');
        aux = operate(aux, '+');
        aux = operate(aux, '-');
        return aux;
    };

    string operate(string aux, char op) {

        double result, a, b;
        int lowerIndex, higherIndex = 0;
        bool cont = false;

        for (int i = 0; i < aux.size(); i++) {
            if (aux[i] == op) {
                cont = true;
                for (int j = i + 1; j < aux.size(); j++) {
                    if ((aux[j] == '0') || (aux[j] == '1') || (aux[j] == '2') || (aux[j] == '3') || (aux[j] == '4') ||
                            (aux[j] == '5') || (aux[j] == '6') || (aux[j] == '7') || (aux[j] == '8') || (aux[j] == '9')||(aux[j]=='.')) {
                        higherIndex = j;
                    } else {
                        j = aux.size();
                    }
                };

                for (int q = i - 1; q >= 0; q--) {
                    if ((aux[q] == '0') || (aux[q] == '1') || (aux[q] == '2') || (aux[q] == '3') || (aux[q] == '4') ||
                            (aux[q] == '5') || (aux[q] == '6') || (aux[q] == '7') || (aux[q] == '8') || (aux[q] == '9')||(aux[q]=='.')) {
                        lowerIndex = q;
                    } else {
                        q = -1;
                    }
                };
                
                a = atof(aux.substr(lowerIndex, i - lowerIndex).c_str());
                b = atof(aux.substr(i + 1, higherIndex - i).c_str());
                
                switch (op) {
                    case '^': result = power(a, b);
                        break;
                    case '*': result = multiply(a, b);
                        break;
                    case '/': result = divide(a, b);
                        break;
                    case '+':
                        result = sum(a, b);
                        break;
                    case '-': result = substract(a, b);
                        break;
                }

                if (lowerIndex == 0)
                    aux = to_string((double) result) + aux.substr(higherIndex + 1, aux.size() - higherIndex - 1);
                else
                    aux = aux.substr(0, lowerIndex) + to_string((double) result) + aux.substr(higherIndex + 1, aux.size() - higherIndex);
            }
            if (cont == true) {
                i = 0;
                cont = false;
            }
        };
        return aux;
    };

    double Calculate(string input) {
        in = input;
        in = '(' + input + ')';
        if ((in == "(q)") || (in == "(Q)")) {
            return 0;
        } else {
            solvePrecedence(in);
            cout << "Please input expression" << endl;
            getline(cin, in);
            Calculate(in);
        }
    };
};

int main(int argc, char** argv) {
    Calculator calc;
    cout << "Welcome, this calculator accepts the following operators" << endl;
    cout << " + , - , * , / , ^ , ( , and )" << endl;
    cout << "Please input expression or press q to end program" << endl;
    string input = "";
    getline(cin, input);
    cout<<calc.Calculate(input)<<endl;;
    cout << "The program has ended, please run again if needed" << endl;
    return 0;
};

